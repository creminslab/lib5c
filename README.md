lib5c
=====

![PyPI - Python Version](https://img.shields.io/pypi/pyversions/lib5c.svg)
![PyPI - License](https://img.shields.io/pypi/l/lib5c)
[![PyPI - Wheel](https://img.shields.io/pypi/wheel/lib5c.svg)](https://pypi.org/project/lib5c)
[![PyPI - Downloads](https://img.shields.io/pypi/dm/lib5c.svg)](https://pypi.org/project/lib5c)
[![Docker Pulls](https://img.shields.io/docker/pulls/creminslab/lib5c.svg)](https://hub.docker.com/r/creminslab/lib5c)
[![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/creminslab/lib5c.svg)](https://bitbucket.org/creminslab/lib5c/addon/pipelines/home)
[![Read the Docs (version)](https://img.shields.io/readthedocs/lib5c/stable.svg)](https://lib5c.readthedocs.io/en/stable)

A library for 5C data analysis.

For complete documentation see https://lib5c.readthedocs.io/

For the latest source, discussion, issues, etc., please visit the Bitbucket
repository at https://bitbucket.org/creminslab/lib5c
