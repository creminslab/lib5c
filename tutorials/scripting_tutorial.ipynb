{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "asax89h2ZLLj"
   },
   "source": [
    "Scripting tutorial\n",
    "==================\n",
    "\n",
    "This tutorial will walk you through how you might leverage the functions exposed in `lib5c` to write your own analysis scripts."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "xJOZ3Ppkdtr4"
   },
   "source": [
    "Follow along in Google colab\n",
    "-----------------\n",
    "\n",
    "You can run and modify the cells in this notebook tutorial live using Google colaboratory by clicking the link below:\n",
    "\n",
    "[![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/github/thomasgilgenast/lib5c-tutorials/blob/master/scripting_tutorial.ipynb)\n",
    "\n",
    "To simply have all the cells run automatically, click `Runtime > Run all` in the colab toolbar."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "IksoZVGkZT0V"
   },
   "source": [
    "Make sure `lib5c` is installed\n",
    "------------------------------\n",
    "\n",
    "Inside a fresh virtual environment, run"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!pip install -q lib5c\n",
    "!lib5c -v"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "P2LL5d5RZYDY"
   },
   "source": [
    "Make a directory and get data\n",
    "-----------------------------\n",
    "\n",
    "If you haven't completed the [pipeline tutorial](pipeline_tutorial.ipynb) yet,\n",
    "make a directory for the tutorial:\n",
    "\n",
    "```\n",
    "$ mkdir lib5c-tutorial\n",
    "$ cd lib5c-tutorial\n",
    "```\n",
    "\n",
    "and prepare the example data in `lib5c-tutorial/input` as shown in the\n",
    "[pipeline tutorial](pipeline_tutorial.ipynb)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 34
    },
    "colab_type": "code",
    "id": "WFPurFIyZVg7",
    "outputId": "f6fd0c4a-93f0-4b1e-ff8b-31d0a8bd72b6"
   },
   "outputs": [],
   "source": [
    "!python -m lib5c.util.demo_data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "juvkosb_Zji3"
   },
   "source": [
    "Note for Docker image users\n",
    "---------------------------\n",
    "\n",
    "If you are using `lib5c` from the Docker image, run\n",
    "\n",
    "    $ docker run -it -v <full path to lib5c-tutorial>:/lib5c-tutorial creminslab/lib5c:latest\n",
    "    root@<container_id>:/# cd /lib5c-tutorial\n",
    "\n",
    "and continue running all tutorial commands in this shell."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "H5-Mo8FNZnUX"
   },
   "source": [
    "Start an interactive session\n",
    "----------------------------\n",
    "\n",
    "To quickly get used to calling the functions in `lib5c`, we recommend trying\n",
    "out the following code snippets in a Python interactive session, which we can\n",
    "start by running\n",
    "\n",
    "    $ python\n",
    "\n",
    "from inside the `lib5c-tutorial` directory.\n",
    "\n",
    "If you're following along in the IPython notebook, you're already in a Python interactive session and don't need to do anything.\n",
    "\n",
    "Of course, you're welcome to write scripts and execute them with commands like\n",
    "\n",
    "    $ python myscript.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "WxXIvTJja_gX"
   },
   "source": [
    "Basic parsing and writing\n",
    "-------------------------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "BND_FntEbAaH"
   },
   "source": [
    "### Parsing\n",
    "\n",
    "In order to perform any operation on our data, we need to load the data into\n",
    "memory first. Many functions for parsing various types of data are exposed in\n",
    "the [lib5c.parsers](https://lib5c.readthedocs.io/en/latest/lib5c.parsers/) subpackage. For this tutorial, we will use the\n",
    "following four parsers:\n",
    "\n",
    "* [lib5c.parsers.primers.load_primermap()](https://lib5c.readthedocs.io/en/latest/lib5c.parsers.primers/#lib5c.parsers.primers.load_primermap) for parsing bedfiles which\n",
    "  describe the genomic coordinates and context of the rows and columns of a\n",
    "  contact matrix into *primermaps*\n",
    "* [lib5c.parsers.counts.load_counts()](https://lib5c.readthedocs.io/en/latest/lib5c.parsers.counts/#lib5c.parsers.counts.load_counts) for parsing countsfiles into\n",
    "  *counts dicts* (dictionaries of contact matrices)\n",
    "\n",
    "For more detail on these data structures and file types, see the section on\n",
    "[Core data structures and file types](https://lib5c.readthedocs.io/en/latest/data_structures_file_types/)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "jsEe430Tb5qf"
   },
   "source": [
    "### Writing\n",
    "\n",
    "After we apply some operations to our data, we often want to save our results by\n",
    "writing data back to the disk. Many functions for writing various types of data\n",
    "to the disk are exposed in the [lib5c.writers](https://lib5c.readthedocs.io/en/latest/lib5c.writers/) subpackage. The writing\n",
    "functions that parallel the parsers listed above are:\n",
    "\n",
    "* [lib5c.writers.primers.write_primermap()](https://lib5c.readthedocs.io/en/latest/lib5c.writers.primers/#lib5c.writers.primers.write_primermap) for writing primer and bin\n",
    "  bedfiles\n",
    "* [lib5c.writers.counts.write_counts()](https://lib5c.readthedocs.io/en/latest/lib5c.writers.counts/#lib5c.writers.counts.write_counts) for writing countsfiles"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "0q6SahtZca2d"
   },
   "source": [
    "Normalization\n",
    "-------------\n",
    "\n",
    "For our first script, we will apply the Knight-Ruiz algorithm to some of our\n",
    "data.\n",
    "\n",
    "First, we need to load information about the primers used in the 5C experiment.\n",
    "We need to do this before we parse the fragment-level raw countsfiles to make\n",
    "sure we parse them correctly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "L0CWpQZdZeVd"
   },
   "outputs": [],
   "source": [
    "from lib5c.parsers.primers import load_primermap\n",
    "primermap = load_primermap('input/BED_ES-NPC-iPS-LOCI_mm9.bed')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "6QWzSUD2cfWK"
   },
   "source": [
    "Now we can parse a countsfile into a counts dict."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "-U1sKRC9cd9Q"
   },
   "outputs": [],
   "source": [
    "from lib5c.parsers.counts import load_counts\n",
    "counts = load_counts('input/pNPC_Rep2.counts', primermap)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "B_OQ8FMucqjM"
   },
   "source": [
    "Notice that we had to pass the `primermap` as an argument to\n",
    "[lib5c.parsers.counts.load_counts()](https://lib5c.readthedocs.io/en/latest/lib5c.parsers.counts/#lib5c.parsers.counts.load_counts).\n",
    "\n",
    "Before balancing the counts matrices, we should remove some low-quality primers\n",
    "which may impair the matrix balancing process. We actually want to do this on\n",
    "the basis of primer quality across all the replicates. To do this, we can\n",
    "leverage the exposed functions [lib5c.algorithms.trimming.trim_primers()](https://lib5c.readthedocs.io/en/latest/lib5c.algorithms.trimming/#lib5c.algorithms.trimming.trim_primers)\n",
    "and [lib5c.algorithms.trimming.trim_counts()](https://lib5c.readthedocs.io/en/latest/lib5c.algorithms.trimming/#lib5c.algorithms.trimming.trim_counts) to do"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "Q4pSL6Ogch96"
   },
   "outputs": [],
   "source": [
    "from lib5c.algorithms.trimming import trim_primers, trim_counts\n",
    "reps = ['v65_Rep1', 'v65_Rep2', 'pNPC_Rep1', 'pNPC_Rep2']\n",
    "counts_superdict = {rep: load_counts('input/%s.counts' % rep, primermap)\n",
    "                    for rep in reps}\n",
    "trimmed_primermap, trimmed_indices = trim_primers(primermap, counts_superdict)\n",
    "trimmed_counts = trim_counts(counts, trimmed_indices)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "yncyvDPCdFJ5"
   },
   "source": [
    "For more details, consult the section on [Trimming](https://lib5c.readthedocs.io/en/latest/trimming/).\n",
    "\n",
    "Now we can balance the counts matrices. To do this, we will use the function\n",
    "[lib5c.algorithms.knight_ruiz.kr_balance_matrix()](https://lib5c.readthedocs.io/en/latest/lib5c.algorithms.knight_ruiz/#lib5c.algorithms.knight_ruiz.kr_balance_matrix). Most algorithms in\n",
    "`lib5c` live in the [lib5c.algorithms](https://lib5c.readthedocs.io/en/latest/lib5c.algorithms/) subpackage and expose some sort\n",
    "of convenience function. To learn more about the various convenience functions\n",
    "and APIs exposed in `lib5c`, consult the section on\n",
    "[API specification and conceptual documentation](https://lib5c.readthedocs.io/en/latest/conceptual/)\n",
    "\n",
    "Go ahead and import this function with"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "E1R4vR_ydBw4"
   },
   "outputs": [],
   "source": [
    "from lib5c.algorithms.knight_ruiz import kr_balance_matrix"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "dGtAobHxde9I"
   },
   "source": [
    "To balance the matrix for the Sox2 region, we can try"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "0QUD8S34degd"
   },
   "outputs": [],
   "source": [
    "kr_counts_Sox2, bias_Sox2, _ = kr_balance_matrix(trimmed_counts['Sox2'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "NgsUVqyVdiDN"
   },
   "source": [
    "To check how balanced the result is, we can immediately check"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 68
    },
    "colab_type": "code",
    "id": "Uiv3Uhzodg_Z",
    "outputId": "97a02578-304c-4d56-8958-ef03b18bab0e"
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "row_sums = np.nansum(kr_counts_Sox2, axis=0)\n",
    "row_sums[:10]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 34
    },
    "colab_type": "code",
    "id": "MDFVKkGneGkw",
    "outputId": "d034076e-0feb-4d82-e05f-550ef19fd5cc"
   },
   "outputs": [],
   "source": [
    "np.max(np.abs(np.mean(row_sums) - row_sums))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "b7SC9Qkgdo-S"
   },
   "source": [
    "For more details on matrix balancing, consult the section on\n",
    "[Bias mitigation](https://lib5c.readthedocs.io/en/latest/bias_mitigation/)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "5_hcyzFFdwNi"
   },
   "source": [
    "### Analyzing multiple regions in parallel\n",
    "\n",
    "Most of the convenience functions exposed in `lib5c` are decorated with a\n",
    "special decorator, `@parallelize_regions`, that parallelizes their operation\n",
    "across the regions of a counts dict. For more information on this decorator, see\n",
    "the section on [Parallelization across regions](https://lib5c.readthedocs.io/en/latest/parallelization_across_regions/).\n",
    "\n",
    "For the purposes of this tutorial, all this means is that we can try something\n",
    "like"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "oKfZVi3edj1d"
   },
   "outputs": [],
   "source": [
    "kr_counts, bias_vectors, _ = kr_balance_matrix(trimmed_counts)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "9qzKAp-fd9HO"
   },
   "source": [
    "to balance all the matrices in the counts dict. To check how balanced one of the\n",
    "matrices is, we can check"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 68
    },
    "colab_type": "code",
    "id": "Csblg0swd753",
    "outputId": "189071fa-dfa7-4183-e366-a66ce7ed725a"
   },
   "outputs": [],
   "source": [
    "row_sums = np.nansum(kr_counts['Klf4'], axis=0)\n",
    "row_sums[:10]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 34
    },
    "colab_type": "code",
    "id": "2CP77X-dd_1J",
    "outputId": "64cb9e89-5e87-4b1e-c3c0-462405c7f414"
   },
   "outputs": [],
   "source": [
    "np.max(np.abs(np.mean(row_sums) - row_sums))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "t8dQ-xySebTC"
   },
   "source": [
    "Notice that the returned object `kr_counts` is a dict indexed by region name,\n",
    "just like the input argument `counts`.\n",
    "\n",
    "Finally, we can save the results of our processing with something like"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 34
    },
    "colab_type": "code",
    "id": "GmoxaH9veZBm",
    "outputId": "6dd92f5f-dc2a-4b62-9c34-08a2034631bc"
   },
   "outputs": [],
   "source": [
    "from lib5c.writers.counts import write_counts\n",
    "write_counts(kr_counts, 'scripting/pNPC_Rep2_kr.counts', trimmed_primermap)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "l_29R4oAegRf"
   },
   "source": [
    "We should save our trimmed primer set as well so that we don't have to redo the\n",
    "trimming step every time we load this data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "ALl6M1e4eekt"
   },
   "outputs": [],
   "source": [
    "from lib5c.writers.primers import write_primermap\n",
    "write_primermap(trimmed_primermap, 'scripting/primers_trimmed.bed')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "elCceRzFekJL"
   },
   "source": [
    "Binning\n",
    "-------\n",
    "\n",
    "If you closed out of the previous session, you'll need to read back in the data\n",
    "we were working with. Try"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "zOX_ybvxeh4a"
   },
   "outputs": [],
   "source": [
    "from lib5c.parsers.primers import load_primermap\n",
    "trimmed_primermap = load_primermap('scripting/primers_trimmed.bed')\n",
    "from lib5c.parsers.counts import load_counts\n",
    "kr_counts = load_counts('scripting/pNPC_Rep2_kr.counts', primermap)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "2Wgdwwy2eoE-"
   },
   "source": [
    "Ultimately, we will want to use the exposed convenience function\n",
    "[lib5c.algorithms.filtering.fragment_bin_filtering.fragment_bin_filter()](https://lib5c.readthedocs.io/en/latest/lib5c.algorithms.filtering.fragment_bin_filtering/#lib5c.algorithms.filtering.fragment_bin_filtering.fragment_bin_filter).\n",
    "According to the docstring, it looks like we will need a `pixelmap` and a\n",
    "`filter_function` in addition to our `counts` dict.\n",
    "\n",
    "The `pixelmap` represents where our bins should be. To generate one, we will\n",
    "use the exposed convenience function\n",
    "[lib5c.algorithms.determine_bins.determine_regional_bins()](https://lib5c.readthedocs.io/en/latest/lib5c.algorithms.determine_bins/#lib5c.algorithms.determine_bins.determine_regional_bins), which can be\n",
    "used to do generate 8 kb bins covering all the regions in our `trimmed_primermap` as follows:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "oTPrLDJQel7x"
   },
   "outputs": [],
   "source": [
    "from lib5c.algorithms.determine_bins import determine_regional_bins\n",
    "pixelmap = determine_regional_bins(\n",
    "    trimmed_primermap, 8000, region_name={r: r for r in primermap.keys()})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "NjtUXQsOfLq3"
   },
   "source": [
    "The `filter_function` represents the filtering function to be passed over the\n",
    "counts matrices in order to determine the value in each bin. To construct one,\n",
    "we can use the exposed convenience function\n",
    "[lib5c.algorithms.filtering.filter_functions.make_filter_function()](https://lib5c.readthedocs.io/en/latest/lib5c.algorithms.filtering.filter_functions/#lib5c.algorithms.filtering.filter_functions.make_filter_function), which\n",
    "can be used to do something like"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "bagxpThCfJvF"
   },
   "outputs": [],
   "source": [
    "from lib5c.algorithms.filtering.filter_functions import make_filter_function\n",
    "filter_function = make_filter_function()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "oJWxd-MKfZYG"
   },
   "source": [
    "Finally, we can bin our counts with a 20 kb window radius by trying"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "XGFzVXZOfX7d"
   },
   "outputs": [],
   "source": [
    "from lib5c.algorithms.filtering.fragment_bin_filtering import \\\n",
    "    fragment_bin_filter\n",
    "binned_counts = fragment_bin_filter(kr_counts, filter_function, pixelmap,\n",
    "                                    trimmed_primermap, 20000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "Akgr1lidfeAk"
   },
   "source": [
    "To save these counts to disk, we can run"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "s-64rfrufbdc"
   },
   "outputs": [],
   "source": [
    "from lib5c.writers.counts import write_counts\n",
    "write_counts(binned_counts, 'scripting/pNPC_Rep2_binned.counts', pixelmap)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "FaP8-y1pfg4g"
   },
   "source": [
    "We can also write the pixelmap we created to the disk as a bin bedfile by trying"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "KuJsIR_JffwZ"
   },
   "outputs": [],
   "source": [
    "from lib5c.writers.primers import write_primermap\n",
    "write_primermap(pixelmap, 'scripting/8kb_bins.bed')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "YtX_uXqPfj1x"
   },
   "source": [
    "For more information about the filtering/binning/smoothing API, see the section\n",
    "on [Binning and smoothing](https://lib5c.readthedocs.io/en/latest/binning_and_smoothing/)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "5dwO1yn9frJz"
   },
   "source": [
    "Plotting heatmaps\n",
    "-----------------\n",
    "\n",
    "If you closed out of the previous session, you'll need to read back in the data\n",
    "we were working with. Try"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "EHIXuQJefiaj"
   },
   "outputs": [],
   "source": [
    "from lib5c.parsers.primers import load_primermap\n",
    "pixelmap = load_primermap('scripting/8kb_bins.bed')\n",
    "from lib5c.parsers.counts import load_counts\n",
    "binned_counts = load_counts('scripting/pNPC_Rep2_binned.counts', pixelmap)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "fgj5XSTlfuwP"
   },
   "source": [
    "To visualize our binned matrices, we can use the exposed function\n",
    "[lib5c.plotters.heatmap.plot_heatmap()](https://lib5c.readthedocs.io/en/latest/lib5c.plotters.heatmap/#lib5c.plotters.heatmap.plot_heatmap).\n",
    "\n",
    "Before we start, it's a good idea to transform the counts values to a log scale\n",
    "for easier visualization"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "LAdnDPLBftgT"
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "logged_counts = {region: np.log(binned_counts[region] + 1)\n",
    "                 for region in binned_counts.keys()}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "OcHxq84Gf5Ky"
   },
   "source": [
    "First, we need to import the plotting function with"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "RoTFb9JPf286"
   },
   "outputs": [],
   "source": [
    "from lib5c.plotters.heatmap import plot_heatmap"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "PAzrlLx7f7uM"
   },
   "source": [
    "We can draw the heatmap for just one region with"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 17
    },
    "colab_type": "code",
    "id": "KX1xsgTmf6dd",
    "outputId": "3ec06aa3-1d0a-4011-d0ea-7b051aad9c2f"
   },
   "outputs": [],
   "source": [
    "%%capture\n",
    "%matplotlib inline\n",
    "plot_heatmap(logged_counts['Sox2'], grange_x=pixelmap['Sox2'], rulers=True,\n",
    "             genes='mm9', colorscale=(1.0, 4.5), colorbar=True,\n",
    "             outfile='scripting/pNPC_Rep2_binned_Sox2.png');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "ixgxB5QKf_Xx"
   },
   "source": [
    "The resulting image should look something like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 468
    },
    "colab_type": "code",
    "id": "YD0mx4B8f-Fw",
    "outputId": "368ce8dc-b254-4330-e26f-6874e995b164"
   },
   "outputs": [],
   "source": [
    "from IPython.display import Image\n",
    "Image(filename='scripting/pNPC_Rep2_binned_Sox2.png', width=500)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "BlboX79bhIXU"
   },
   "source": [
    "Because [plot_heatmap()](https://lib5c.readthedocs.io/en/latest/lib5c.plotters.heatmap/#lib5c.plotters.heatmap.plot_heatmap) is parallelized with the `@parallelize_regions`\n",
    "decorator, we can draw the heatmaps for all regions at once by simply calling"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 52
    },
    "colab_type": "code",
    "id": "RrCspUjtg8Ys",
    "outputId": "92717bb9-ac84-43d9-cd53-de460514ad1f"
   },
   "outputs": [],
   "source": [
    "%%capture\n",
    "%matplotlib inline\n",
    "outfile_names = {region: 'scripting/pNPC_Rep2_binned_%s.png' % region\n",
    "                 for region in binned_counts.keys()}\n",
    "plot_heatmap(logged_counts, grange_x=pixelmap, rulers=True, genes='mm9',\n",
    "             colorscale=(1.0, 4.5), colorbar=True, outfile=outfile_names);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "-EWdyxzphXdJ"
   },
   "source": [
    "where we precompute a dict of output filenames (parallel to `counts_binned`)\n",
    "to describe where each region's heatmap will get drawn (since each region will\n",
    "be drawn to a separate heatmap). In this way, any argument to a parallelized\n",
    "function can be replaced by a dict whose keys match the keys of the first\n",
    "positional argument (which is usually a counts dict).\n",
    "\n",
    "You can show the plot directly inline in a notebook environment by skipping the `outfile` kwarg:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 324
    },
    "colab_type": "code",
    "id": "DjwedWLZpQrJ",
    "outputId": "174096e5-a08d-4f63-e056-576f4482990f"
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "plot_heatmap(logged_counts['Sox2'], grange_x=pixelmap['Sox2'], rulers=True,\n",
    "             genes='mm9', colorscale=(1.0, 4.5), colorbar=True);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "MiVt6eXnhbLl"
   },
   "source": [
    "Expected modeling\n",
    "-----------------\n",
    "\n",
    "The exposed convenience function for making an expected model is\n",
    "[lib5c.algorithms.expected.make_expected_matrix()](https://lib5c.readthedocs.io/en/latest/lib5c.algorithms.expected/#lib5c.algorithms.expected.make_expected_matrix).\n",
    "\n",
    "To construct an expected model for each region using a simple power law\n",
    "relationship and apply donut correction in the same step, we can try"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 87
    },
    "colab_type": "code",
    "id": "ptUg_vqJhRWr",
    "outputId": "0ea1c356-3703-4972-f15a-e2a401dd11d4"
   },
   "outputs": [],
   "source": [
    "from lib5c.algorithms.expected import make_expected_matrix\n",
    "exp_counts, dist_exp, _ = make_expected_matrix(\n",
    "    binned_counts, regression=True, exclude_near_diagonal=True, donut=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "USNzcTOmhtwi"
   },
   "source": [
    "The first returned value, `counts_exp`, is simply a dict of the expected\n",
    "matrices representing the model. The second returned value, `dist_exp`, is a\n",
    "representation of the simple one-dimensional expected model.\n",
    "\n",
    "We can visualize the one-dimensional expected model using\n",
    "[lib5c.plotters.expected.plot_bin_expected()](https://lib5c.readthedocs.io/en/latest/lib5c.plotters.expected/#lib5c.plotters.expected.plot_bin_expected)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 35
    },
    "colab_type": "code",
    "id": "M-TZxowYhq_8",
    "outputId": "1138b9d8-b17b-4dbe-97f5-1b4ffbab7aac"
   },
   "outputs": [],
   "source": [
    "%%capture\n",
    "%matplotlib inline\n",
    "from lib5c.plotters.expected import plot_bin_expected\n",
    "plot_bin_expected(binned_counts['Sox2'], dist_exp['Sox2'],\n",
    "                  outfile='scripting/pNPC_Rep2_expected_model_Sox2.png',\n",
    "                  hexbin=True, semilog=True, xlabel='distance');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "YsX6FspkpqBf"
   },
   "source": [
    "or for all regions at once,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 52
    },
    "colab_type": "code",
    "id": "FXhnaLtmh5Lu",
    "outputId": "68a958cc-4835-40ce-9617-abc038953680"
   },
   "outputs": [],
   "source": [
    "%%capture\n",
    "%matplotlib inline\n",
    "from lib5c.plotters.expected import plot_bin_expected\n",
    "outfile_names = {region: 'scripting/pNPC_Rep2_expected_model_%s.png' % region\n",
    "                 for region in binned_counts.keys()}\n",
    "plot_bin_expected(binned_counts, dist_exp, outfile=outfile_names, hexbin=True,\n",
    "                  semilog=True, xlabel='distance');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "rpMDoD1Wp1Ws"
   },
   "source": [
    "The kwargs `hexbin`, `semilog`, and `ylabel` tweak the visual appearance\n",
    "of the resulting plot.\n",
    "\n",
    "The resulting images should look something like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 386
    },
    "colab_type": "code",
    "id": "tYXfxXwhpwnj",
    "outputId": "f360cfa1-c74d-4f76-c7ec-042f6df0810b"
   },
   "outputs": [],
   "source": [
    "Image(filename='scripting/pNPC_Rep2_expected_model_Sox2.png', width=500)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "Jtb-Of0zp70_"
   },
   "source": [
    "To learn more, consult the section on [Expected modeling](https://lib5c.readthedocs.io/en/latest/expected_modeling/)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "rJ_RJe2dqOiP"
   },
   "source": [
    "Variance modeling\n",
    "-----------------\n",
    "\n",
    "The exposed convenience functions for variance modeling is\n",
    "[lib5c.algorithms.variance.estimate_variance()](https://lib5c.readthedocs.io/en/latest/lib5c.algorithms.variance.estimate_variance/#lib5c.algorithms.variance.estimate_variance).\n",
    "\n",
    "We can get variance estimates from a log-normal, deviation-based distance-variance relationship model by trying"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "nNmVhVLVqsoA"
   },
   "outputs": [],
   "source": [
    "from lib5c.algorithms.variance import estimate_variance\n",
    "var_counts = estimate_variance(binned_counts, exp_counts)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "hJ-0WuQ8qwQG"
   },
   "source": [
    "To learn more, consult the section on [Variance modeling](https://lib5c.readthedocs.io/en/latest/variance_modeling/)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "8n9VxxfPq7ZY"
   },
   "source": [
    "P-value calling\n",
    "---------------\n",
    "\n",
    "The exposed convenience function for calling p-values using distributions\n",
    "parametrized according to the expected and variance models is\n",
    "[lib5c.util.distributions.call_pvalues()](https://lib5c.readthedocs.io/en/latest/lib5c.util.distributions/#lib5c.util.distributions.call_pvalues).\n",
    "\n",
    "To simply call the p-values using a log-normal distribution, we can try"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "K45XND0iqtlC"
   },
   "outputs": [],
   "source": [
    "from lib5c.util.distributions import call_pvalues\n",
    "from lib5c.util.counts import parallel_log_counts\n",
    "pvalues = call_pvalues(parallel_log_counts(binned_counts), exp_counts,\n",
    "                       var_counts, 'norm', log=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "7qsRcm-CrXa5"
   },
   "source": [
    "Where we are logging the observed counts and comparing them to a normal distribution. \n",
    "\n",
    "We can visualize these called p-values as interaction scores\n",
    "(`-10*log2(pvalue)`) on heatmaps by calling"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 52
    },
    "colab_type": "code",
    "id": "QEjZ8NWnrlN5",
    "outputId": "8af9d2c4-8f1d-4db3-b1a8-3ec6efbba075"
   },
   "outputs": [],
   "source": [
    "%%capture\n",
    "%matplotlib inline\n",
    "from lib5c.plotters.heatmap import plot_heatmap\n",
    "from lib5c.util.counts import convert_pvalues_to_interaction_scores\n",
    "outfile_names = {region: 'scripting/pNPC_Rep2_is_%s.png' % region\n",
    "                 for region in pvalues.keys()}\n",
    "interaction_scores = convert_pvalues_to_interaction_scores(pvalues)\n",
    "plot_heatmap(interaction_scores, grange_x=pixelmap, rulers=True, #genes='mm9',\n",
    "             colorscale=(0, 300), colorbar=True, colormap='is',\n",
    "             outfile=outfile_names);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "4iVobZIErkfJ"
   },
   "source": [
    "and write the called p-values to the disk as counts files by calling"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {},
    "colab_type": "code",
    "id": "63puFaUpsWlE"
   },
   "outputs": [],
   "source": [
    "from lib5c.writers.counts import write_counts\n",
    "write_counts(pvalues, 'scripting/pNPC_Rep2_pvalues.counts', pixelmap)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "HtwKld1AsarD"
   },
   "source": [
    "The resulting heatmaps should look something like this"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 450
    },
    "colab_type": "code",
    "id": "Eewlm9mmsYk4",
    "outputId": "1203f6bb-f07f-4a46-8dda-842340213868"
   },
   "outputs": [],
   "source": [
    "Image(filename='scripting/pNPC_Rep2_is_Sox2.png', width=500)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "4UKW_uuzs4LX"
   },
   "source": [
    "For more information on the distribution fitting and p-value calling API, see\n",
    "the section on [Distributions](https://lib5c.readthedocs.io/en/latest/distributions/)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "colab_type": "text",
    "id": "fVeaLINEtAMO"
   },
   "source": [
    "Next steps\n",
    "----------\n",
    "\n",
    "This tutorial shows only a few of the functions exposed in the `lib5c` API.\n",
    "You can read more about what you can do with `lib5c` in the section on\n",
    "[API specification and conceptual documentation](https://lib5c.readthedocs.io/en/latest/conceptual/)."
   ]
  }
 ],
 "metadata": {
  "colab": {
   "collapsed_sections": [
    "fVeaLINEtAMO"
   ],
   "name": "scripting_tutorial.ipynb",
   "provenance": [],
   "version": "0.3.2"
  },
  "kernel_info": {
   "name": "python2"
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.8"
  },
  "nteract": {
   "version": "0.12.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
