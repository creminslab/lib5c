import os
import subprocess
import glob
import shutil


def main():
    nbs = glob.glob('tutorials/*.ipynb')
    for nb in nbs:
        cmd = 'nbstripout %s' % nb
        print(cmd)
        subprocess.call(cmd, shell=True)
    try:
        os.remove('tutorials/luigi.cfg')
    except OSError:
        pass
    shutil.rmtree('tutorials/input', ignore_errors=True)
    shutil.rmtree('tutorials/raw', ignore_errors=True)
    shutil.rmtree('tutorials/trimmed', ignore_errors=True)
    shutil.rmtree('tutorials/kr', ignore_errors=True)
    shutil.rmtree('tutorials/bedfiles', ignore_errors=True)
    shutil.rmtree('tutorials/binned', ignore_errors=True)
    shutil.rmtree('tutorials/expected', ignore_errors=True)
    shutil.rmtree('tutorials/variance', ignore_errors=True)
    shutil.rmtree('tutorials/expected', ignore_errors=True)
    shutil.rmtree('tutorials/pvalues', ignore_errors=True)
    shutil.rmtree('tutorials/scripting', ignore_errors=True)


if __name__ == '__main__':
    main()
