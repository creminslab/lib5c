import io
import sys
import os
import glob

import nbformat
from nbconvert.preprocessors import ExecutePreprocessor


def _notebook_run(path):
    kernel_name = 'python%d' % sys.version_info[0]
    with io.open(path, 'r') as handle:
        nb = nbformat.read(handle, as_version=4)
        nb.metadata.get('kernelspec', {})['name'] = kernel_name
        ep = ExecutePreprocessor(kernel_name=kernel_name, timeout=None)
        ep.preprocess(nb, {'metadata': {'path': os.path.dirname(__file__)}})
    return nb


def main():
    nbs = glob.glob('tutorials/*.ipynb')
    for nb in nbs:
        print(nb)
        res_nb = _notebook_run(nb)
        with io.open(nb, 'w') as handle:
            nbformat.write(res_nb, handle)


if __name__ == '__main__':
    main()
