Expected modeling
=================

.. toctree::
    :hidden:

5C data exhibit a strong distance-dependent background signal which is also
influenced by local contact domain structure. ``lib5c`` includes a variety of
algorithms for modeling this background expected interaction frequency.

Command-line interfaces
-----------------------

The subcommand is
::

    $ lib5c expected

For detailed help, run
::

    $ lib5c expected -h

Exposed functionality
---------------------

The subpackage responsible for expected modeling is :mod:`lib5c.algorithms.expected`.

The most important exposed function is :func:`lib5c.algorithms.expected.make_expected_matrix`.

To use a global expected model, you must also use :func:`lib5c.algorithms.expected.get_global_distance_expected`,
leading to the following workflow::

    from lib5c.algorithms.expected import make_expected_matrix, get_global_distance_expected

    distance_expected = get_global_distance_expected(observed_counts)
    expected_counts = make_expected_matrix(observed_counts, distance_expected=distance_expected)

The following functions are provided for visualizing one-dimensional expected
models overlayed over real data:

* :func:`lib5c.plotters.expected.plot_bin_expected`
* :func:`lib5c.plotters.expected.plot_fragment_expected`
