Enrichments
===========

.. toctree::
    :hidden:

An important endgame step in 5C analysis is the quantification of overlap
between specific interaction classes and traditional genomic annotations.
``lib5c`` provides a system for performing such an enrichment analysis.

Command-line interface
----------------------

To plot traditional enrichments, run
::

    $ lib5c plot enrichment

To plot the enrichment of occupied motifs with a specific orientation, run
::

    $ lib5c plot convergency

Exposed functionality
---------------------

The exposed functions for computing enrichments are:

* :func:`lib5c.algorithms.enrichment.count_intersections_all`
* :func:`lib5c.algorithms.enrichment.get_annotation_percentage_all`
* :func:`lib5c.algorithms.enrichment.get_fold_change_all`
* :func:`lib5c.algorithms.enrichment.get_fisher_exact_pvalue_all`
* :func:`lib5c.algorithms.convergency.compute_convergency`

The exposed functions for visualization are:

* :func:`lib5c.plotters.enrichment.plot_looptype_vs_annotation_heatmap`
* :func:`lib5c.plotters.enrichment.plot_annotation_vs_annotation_heatmap`
* :func:`lib5c.plotters.enrichment.plot_stack_bargraph`
* :func:`lib5c.plotters.convergency.plot_convergency`
