Distributions
=============

.. toctree::
    :hidden:

In order to call p-values for individual 5C interactions, we simply compare the
observed value of the interaction to its expected value
(see :doc:`Expected modeling <expected_modeling>`) and its variance estimate
(see :doc:`Variance modeling <variance_modeling>`) by using a statistical
distribution parameterized to have mean and variance equal to the predictions of
the expected and variance models, respectively.

Conceptual overview
-------------------

We may wish to call p-values using a variety of different statistical
distributions, each of which has a unique "native" parameterization. To keep
things from becoming too complicated, we ignore these "native" parameterizations
and instead always parameterize distributions with the same two parameters:
mean and variance.

Command-line interfaces
-----------------------

To call p-values, run
::

    $ lib5c pvalues

To plot the parameterized distributions over the real data, run
::

    $ lib5c plot visualize-fits

Exposed functionality
---------------------

The exposed function is :func:`lib5c.util.distributions.call_pvalues`.

Additional utility functions are provided in :mod:`lib5c.util.distributions` to
ease parameter conversion, etc.

The following functions are exposed for visualization of distributions:

* :func:`lib5c.plotters.fits.plot_fit`
* :func:`lib5c.plotters.fits.plot_group_fit`

``plot_fit()`` is a reusable function for overlaying distributions.

``plot_group_fit()`` is a convenience function for overlaying parametrized
distributions over groups of real data points.
