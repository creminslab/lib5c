Thresholding
============

.. toctree::
    :hidden:

After calling p-values for interactions in each of several replicates, we want
to find a way to combine this information across all replicates to identify
condition-specific interactions. ``lib5c`` provides a simple implementation of
such a procedure.

Conceptual background
---------------------

We assume each replicate belongs to one of exactly two conditions, call them "A"
and "B". This implies that there are three important classes of interactions we
wish to recover:

* "A-only" (looping in condition A but not in condition B)
* "B-only" (looping in condition B but not in condition A)
* "constitutive" (looping in both conditions)

We will also recover a "background" class, which will be convenient when
assessing :doc:`enrichments <enrichments>`.

Command-line interface
----------------------

To perform the simple two-way thresholding, run
::

    $ lib5c threshold

Exposed functionality
---------------------

The subpackage for thresholding is :mod:`lib5c.algorithms.thresholding`.

The main exposed function is :func:`lib5c.algorithms.thresholding.two_way_thresholding`.

The following functions are exposed to compute various useful metrics from the
results of ``two_way_thresholding()``:

* :func:`lib5c.algorithms.thresholding.kappa`
* :func:`lib5c.algorithms.thresholding.concordance_confusion`
* :func:`lib5c.algorithms.thresholding.color_confusion`
* :func:`lib5c.algorithms.thresholding.count_clusters`
