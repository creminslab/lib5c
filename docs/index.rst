lib5c
=====

A library for Chromosome Conformation Capture Carbon Copy (5C) analysis.

Contents:

.. toctree::
    :maxdepth: 1

    installation
    introduction
    Pipeline tutorial <https://colab.research.google.com/github/thomasgilgenast/lib5c-tutorials/blob/master/pipeline_tutorial.ipynb>
    Command line tutorial <https://colab.research.google.com/github/thomasgilgenast/lib5c-tutorials/blob/master/commandline_tutorial.ipynb>
    Scripting tutorial <https://colab.research.google.com/github/thomasgilgenast/lib5c-tutorials/blob/master/scripting_tutorial.ipynb>
    conceptual
    test_build_release
    Changelog <changelog>
    Detailed package reference <modules>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
