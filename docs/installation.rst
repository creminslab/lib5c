Installation
============

.. toctree::
    :hidden:

``lib5c`` can be installed directly via ``pip``
::

    $ pip install lib5c

If you are having trouble with this step, make sure ``pip`` is up to date by
running
::

    $ pip install --upgrade pip

Prerequisites
-------------

``lib5c`` currently only supports Python 2.7.11+ or Python 3.6+.

``pip`` will install all required prerequisites automatically.

The complete list of required dependencies is listed in ``install_requires`` in
``setup.py``.

Virtualenv installation walkthrough
-----------------------------------

It is highly recommended to install ``lib5c`` inside a fresh virtual environment
to avoid package version conflicts.

If you don't have ``virtualenv`` you may install it via
::

    $ pip install virtualenv

The step-by-step procedure for installing ``lib5c`` inside a fresh virtual
environment is then::

    $ deactivate                # deactivate any existing virtualenv
    $ virtualenv venv           # create new python2 virtualenv
    $ source venv/bin/activate  # activate the new virtualenv
    (venv)$ pip install -U pip  # make sure pip is up to date
    (venv)$ pip install lib5c   # install lib5c
    (venv)$ lib5c -v            # check version number

On Windows, the virtualenv can be activated by running::

    > venv\Scripts\activate

instead.

Docker image
------------

A Docker image for ``lib5c`` is also provided and can be used as shown here::

    $ docker pull creminslab/lib5c:latest
    $ docker run -it creminslab/lib5c:latest
    root@<container_id>:/# lib5c -v

We also provide a ``creminslab/lib5c:slim`` Docker image which is based on
``python:2.7-slim``.

It is recommended to bind a directory containing input files. For example::

    $ docker run -it -v c:/data:/data creminslab/lib5c:latest
    root@<container_id>:/# cd /data
    root@<container_id>:/data# lib5c pipeline

Optional dependencies
---------------------

You can install ``lib5c`` with all optional dependencies included by running
::

    $ pip install lib5c[complete]

The individual optional dependencies can also be installed one-at-a-time; they
are described in detail in the following sub-sections.

``bsub`` integration
^^^^^^^^^^^^^^^^^^^^

If you use the LSF job scheduling system (also known as bsub), you should also
install the ``bsub`` package to enable ``lib5c`` to leverage the job scheduler.
::

    $ pip install bsub>=0.3.5

ICE matrix balancing
^^^^^^^^^^^^^^^^^^^^

If you want to use the ICE matrix balancing algorithm in your analyses, you
should also install the ``iced`` package to enable it in ``lib5c``.
::

    $ pip install iced>=0.4.0

This package may be difficult or impossible to install on Windows systems, in
which case you can use the Knight-Ruiz matrix balancing algorithm as an
alternative.

BigWig file support
^^^^^^^^^^^^^^^^^^^

In order to support interaction with BigWig formatted files, you should also
install the ``pyBigWig`` package to enable it in ``lib5c``.
::

    $ pip install pyBigWig>=0.3.4

This package may be difficult or impossible to install on Windows systems.

Special notes
-------------

``lib5c`` plots output using the ``matplotlib`` Python package. Instead of
forcing a particular backend to be used, ``lib5c`` will respect the backend set
in your ``matplotlibrc``. As an example, the following ``matplotlibrc`` sets the
backend to 'agg'::

    $ cat ~/.config/matplotlib/matplotlibrc
    backend: agg

On Windows systems the ``matplotlibrc`` is usually located at
``C:\Users\<username>\.matplotlib\matplotlibrc``

Another way to set your default backend is to use the ``MPLBACKEND`` environment
variable::

    $ export MPLBACKEND=agg

You can add this line to your ``~/.bashrc`` to set this environment variable on
every login.

Editable mode installation
--------------------------

To install lib5c in editable mode::

    $ git clone https://bitbucket.org/creminslab/lib5c
    $ pip install -e ./lib5c

We recommend doing this is in a clean virtualenv.

To ensure that version information (e.g., as displayed by running ``lib5c -v``
on the command line) of an editable install of lib5c remains correct, install
setuptools-scm::

    $ pip install setuptools-scm
