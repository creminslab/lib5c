Quantile normalization
======================

.. toctree::
    :hidden:

Quantile normalization is a method for bringing disparate 5C libraries (of
varying sequencing depth, library complexity, etc.) to a comparable scale.

Command-line interfaces
-----------------------

The subcommand is
::

    $ lib5c qnorm

For detailed help, run
::

    $ lib5c qnorm -h

Exposed functionality
---------------------

The subpackage for quantile normalization is :mod:`lib5c.algorithms.qnorm`

Two functions are exposed:

* :func:`lib5c.algorithms.qnorm.qnorm`
* :func:`lib5c.algorithms.qnorm.qnorm_counts_superdict`

``qnorm()`` performs quantile normalization on arbitrary data, and may be
re-used in other contexts. It operates on a table of input data, not the
counts dicts (and superdicts) which are commonly used in the library. Therefore,
the wrapper function ``qnorm_counts_superdict()`` is provided to make this step
easier for our 5C data.
