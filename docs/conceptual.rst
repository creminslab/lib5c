API specification and conceptual documentation
==============================================

This section describes the most important data structures and functions in more
detail than the tutorial.

Contents:

.. toctree::
    :maxdepth: 1

    data_structures_file_types
    parallelization_across_regions
    plotting
    trimming
    quantile_normalization
    bias_mitigation
    binning_and_smoothing
    expected_modeling
    variance_modeling
    distributions
    thresholding
    enrichments
    clustering
