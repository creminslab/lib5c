Variance modeling
=================

.. toctree::
    :hidden:

In order to obtain measures of statistical significance for 5C interactions, we
need a quantitative measure of statistical noise for each interaction. ``lib5c``
provides a variety of methods for estimating this variance.

Command-line interfaces
-----------------------

To model the variance, run
::

    $ lib5c variance

To visualize variance estimates, run
::

    $ lib5c plot visualize-variance

Exposed functionality
---------------------

The subpackage responsible for variance modeling is
:mod:`lib5c.algorithms.variance`.

The top-level convenience function exposed is
:func:`lib5c.algorithms.variance.estimate_variance`

``estimate_variance()`` provides easy access to all available variance
estimation methods.

For an example of how to visualize variance estimates using
:func:`lib5c.plotters.scatter.scatter` or
:func:`lib5c.plotters.curve_fits.plot_fit`, take a look at
:func:`lib5c.tools.visualize_variance.visualize_variance_tool`.
