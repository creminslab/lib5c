Distribution fitting
====================

.. toctree::
    :hidden:

An essential part of the 5C analysis process is the assignment of a p-value to
the interaction between the ``i`` th and ``j`` th locus in a region. In order to
do this, we typically construct a theoretical statistical distribution from a
specified family of distributions, and either parameterize or fit a collection
of distributions.

Theoretical overview
--------------------

We define four different approaches to assigning p-values:

1. ``regional_simple``
2. ``regional_shifted``
3. ``obs_over_exp``
4. ``log_log_fit``

The first three of these are statistically equivalent, and among them
``obs_over_exp`` is almost always preferred. ``obs_over_exp`` and
``log_log_fit`` differ by how they estimate a mean-variance scaling parameter,
but are conceptually very similar.

``regional_simple`` approach
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This approach works by performing the following steps:

- Compute an "observed over expected" matrix via element-wise division.
- Log this matrix if appropriate (e.g., log-normal and log-logistic
  distributions).
- Fit a single distribution to the "observed over expected" matrix.
- Evaluate the "observed over expected" value at each point against this
  distribution.

``regional_shifted`` approach
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This approach works by performing the following steps:

 - Shift the observed counts for the entire region to the expected at this
   point using ``shifted_matrix = regional_obs * (exp_value / regional_exp)``.
 - Log this matrix if appropriate (e.g., log-normal and log-logistic
   distributions).
 - Fit a distribution to the shifted matrix.
 - Evaluate the observed value (logging if appropriate) against this
   distribution.

``log_log_fit`` and ``obs_over_exp`` approaches
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

These approaches do not fit any distributions, and instead guess distributional
parameters from the mean and variance of the observed counts in the region.

The variance at a point is assumed to be
::

    sigma_2 = variance_factor * exp_value**2

and the corresponding mean is assumed to be
::

    mu = mean_factor * exp_value

where ``exp_value`` is the expected value at that point, as read in from the
expected model.

In log space, these equations are slightly different::

    mu = np.log(exp_value) + mean_factor
    sigma_2 = variance_factor

The ``mean_factor`` is optional and is assumed to be 1 (no effect) by default.

Command-line interfaces
-----------------------

Command-line interfaces for assigning p-values, visualizing fits, and
visualizing variance estimates are provided directly in ``lib5c``.

Assigning p-values
~~~~~~~~~~~~~~~~~~

If we have a countsfile of observed values called ``obs.counts``, a matching
countsfile of expected values called ``exp.counts``, and a bedfile describing
the loci in the countsfiles called ``bins.bed``, we can call p-values by running
::

    $ lib5c pvalues -p bins.bed obs.counts exp.counts pvalues.counts

For a complete list of command-line flags for the ``lib5c pvalues`` subcommand,
we can run
::

    $ lib5c pvalues -h

Visualizing fits
~~~~~~~~~~~~~~~~

To visualize fits at a distance scale of 200 kb, we can run
::

    $ lib5c visualize-fits -s 200000 -p bins.bed obs.counts exp.counts fits_200kb_%r.png

where ``%r`` will get replaced by the region name.

For a complete list of command-line flags for the ``lib5c visualize-fits``
subcommand, we can run
::

    $ lib5c visualize-fits -h

Visualizing variance estimates
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To visualize the effective variance estimates begin used by our p-value calling
procedure, we can run
::

    $ lib5c visualize-variance -p bins.bed obs.counts exp.counts variance_%r.png

where ``%r`` will get replaced by the region name.

For a complete list of command-line flags for the ``lib5c visualize-variance``
subcommand, we can run
::

    $ lib5c visualize-variance -h

Exposed functionality
---------------------

The algorithms which make up the distribution fitting framework can be found in
the :mod:`lib5c.algorithms.distributions` subpackage.

Core API
~~~~~~~~

The core API for assigning p-values is the single parallelized convenience
function

:func:`lib5c.algorithms.distributions.fitting.fit_and_call`

which takes in an observed matrix and an expected matrix and returns a matrix of
called p-values.

The details of the fitting procedure can be customized by passing the
appropriate kwargs. See the function docstring for more information.
