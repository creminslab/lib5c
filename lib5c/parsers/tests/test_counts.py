"""
Unit tests for lib5c.parsers.counts.
"""

import unittest

import numpy as np

from lib5c.parsers.counts import load_counts
from lib5c.parsers.primers import load_primermap


class CountsTestCase(unittest.TestCase):
    def setUp(self):
        # load a primermap and pixelmap
        self.pixelmap = load_primermap('test/bins.bed')
        self.primermap = load_primermap('test/primers.bed')

    def test_load_counts(self):
        # load counts
        counts = load_counts('test/test.counts', self.pixelmap)

        # check keys
        self.assertTrue('Klf4' in counts.keys())
        self.assertEqual(len(counts.keys()), 7)

        # check size of array
        self.assertEqual(len(counts['Klf4']), 82)
        self.assertEqual(len(counts['Klf4'][0]), 82)

        # check one count value
        self.assertEqual(counts['Klf4'][0][0], 1.02489)

        # check symmetry of matrix
        self.assertEqual(counts['Klf4'][1][0], -0.20552)
        self.assertEqual(counts['Klf4'][0][1], -0.20552)

    def test_load_primer_counts(self):
        # load counts
        counts = load_counts('test/test_raw.counts', self.primermap)

        # check keys
        self.assertTrue('Klf4' in counts.keys())
        self.assertEqual(len(counts.keys()), 7)

        # check size of array
        self.assertEqual(len(counts['Klf4']), 251)
        self.assertEqual(len(counts['Klf4'][0]), 251)

        # check random interaction
        self.assertTrue(np.isnan(counts['Klf4'][75][75]))

        # check symmetry of matrix
        self.assertEqual(counts['Klf4'][75][142], 17.0)
        self.assertEqual(counts['Klf4'][142][75], 17.0)

        # check specific interaction
        index_one = [p['name'] for p in self.primermap['gene-desert']]\
            .index('5C_331_gene-desert_REV_13')
        index_two = [p['name'] for p in self.primermap['gene-desert']]\
            .index('5C_331_gene-desert_FOR_47')
        self.assertEqual(counts['gene-desert'][index_one, index_two], 65.0)
