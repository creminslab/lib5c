"""
Unit tests for lib5c.parsers.primers.
"""

import unittest

from lib5c.parsers.primers import load_primermap


class PrimersTestCase(unittest.TestCase):
    def test_get_pixelmap(self):
        # get pixelmap
        pixelmap = load_primermap('test/bins.bed')

        # check keys
        self.assertEqual(len(pixelmap.keys()), 7)
        self.assertTrue('Nestin' in pixelmap.keys())

        # check first bin
        self.assertEqual(pixelmap['Nestin'][0], {'start': 87287087,
                                                 'end': 87299087,
                                                 'name': 'Nestin_BIN_000',
                                                 'index': 0,
                                                 'region': 'Nestin',
                                                 'chrom': 'chr3'})

    def test_get_primermap(self):
        # get primermap
        primermap = load_primermap('test/primers.bed')

        # check keys
        self.assertEqual(len(primermap.keys()), 7)
        self.assertTrue('Nestin' in primermap.keys())

        # check first bin
        self.assertEqual(primermap['Nestin'][0], {'end'         : 87285637,
                                                  'name'        :
                                                      '5C_326_Nestin_REV_9',
                                                  'region'      : 'Nestin',
                                                  'number'      : 9,
                                                  'start'       : 87282063,
                                                  'chrom'       : 'chr3',
                                                  'strand'      : '-',
                                                  'orientation': "5'"})

    def test_get_primermap_on_pixelmap(self):
        # get primermap
        pixelmap = load_primermap('test/bins.bed')

        # check keys
        self.assertEqual(len(pixelmap.keys()), 7)
        self.assertTrue('Nestin' in pixelmap.keys())

        # check first bin
        self.assertEqual(pixelmap['Nestin'][0], {'start' : 87287087,
                                                 'end'   : 87299087,
                                                 'name'  : 'Nestin_BIN_000',
                                                 'index' : 0,
                                                 'region': 'Nestin',
                                                 'chrom' : 'chr3'})

    def test_get_primermap_with_nuc(self):
        # get primermap
        primermap = load_primermap('test/primers_nuc.bed')

        # check keys
        self.assertEqual(len(primermap.keys()), 7)
        self.assertTrue('Nestin' in primermap.keys())

        # check first bin
        self.assertEqual(primermap['Nestin'][0], {'8_usercol' : 87285636,
                                                  '15_num_T'  : 953,
                                                  '16_num_N'  : 0,
                                                  'end'       : 87285637,
                                                  'name'      :
                                                      '5C_326_Nestin_REV_9',
                                                  '13_num_C'  : 892,
                                                  '12_num_A'  : 956,
                                                  '14_num_G'  : 772,
                                                  'region'    : 'Nestin',
                                                  '10_pct_at' : 0.534285,
                                                  'number'    : 9,
                                                  '7_usercol' : 87282063,
                                                  '18_seq_len': 3573,
                                                  '17_num_oth': 0,
                                                  'start'     : 87282063,
                                                  '11_pct_gc' : 0.465715,
                                                  '5_usercol' : 0,
                                                  '9_usercol' : '255,0,0',
                                                  '6_usercol' : '-',
                                                  'chrom'     : 'chr3',
                                                  'strand'    : '-',
                                                  'orientation': "5'"})
