"""
Unit tests for lib5c.parsers.genes.
"""

import unittest

from lib5c.parsers.genes import load_genes, load_gene_table


class GenesTestCase(unittest.TestCase):
    def test_load_genes(self):
        # load genes
        genes = load_genes('test/genes.bed')

        # check number of genes read
        self.assertEqual(len(genes['chr3']), 65)

        # check first gene
        self.assertEqual(genes['chr3'][0], {
            'chrom': 'chr3',
            'start': 87239703,
            'end': 87304600,
            'blocks': [{'start': 87239806, 'end': 87239870},
                       {'start': 87244109, 'end': 87244130},
                       {'start': 87247455, 'end': 87247716},
                       {'start': 87247978, 'end': 87248263},
                       {'start': 87250166, 'end': 87250448},
                       {'start': 87252080, 'end': 87252359},
                       {'start': 87258545, 'end': 87258653},
                       {'start': 87259673, 'end': 87259701},
                       {'start': 87261029, 'end': 87261112},
                       {'start': 87261291, 'end': 87261354},
                       {'start': 87261705, 'end': 87261758}],
            'name': 'NM_001113238',
            'strand': '+'})

    def test_load_gene_table(self):
        # load genes
        genes = load_gene_table(
            'lib5c/plotters/gene_tracks/mm9_refseq_genes.gz')

        # spot check a gene
        special_gene = None
        for gene in genes['chr3']:
            if gene['id'] == 'NM_001113238':
                special_gene = gene
                break
        self.assertEqual(special_gene, {
            'chrom' : 'chr3',
            'start' : 87239703,
            'end'   : 87304600,
            'blocks': [{'start': 87239806, 'end': 87239870},
                       {'start': 87244109, 'end': 87244130},
                       {'start': 87247455, 'end': 87247716},
                       {'start': 87247978, 'end': 87248263},
                       {'start': 87250166, 'end': 87250448},
                       {'start': 87252080, 'end': 87252359},
                       {'start': 87258545, 'end': 87258653},
                       {'start': 87259673, 'end': 87259701},
                       {'start': 87261029, 'end': 87261112},
                       {'start': 87261291, 'end': 87261354},
                       {'start': 87261705, 'end': 87261758}],
            'name'  : 'Fcrl5',
            'id'    : 'NM_001113238',
            'strand': '+'})
