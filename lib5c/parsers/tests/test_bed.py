"""
Unit tests for lib5c.parsers.bed.
"""

import unittest

from lib5c.parsers.bed import load_features


class BedTestCase(unittest.TestCase):
    def test_load_features_with_id_and_value(self):
        # load features
        peaks = load_features('test/peaks.bed', id_index=3, value_index=4)

        # check number of features loaded
        self.assertEqual(len(peaks['chr1']), 982)

        # check first feature to confirm we read all the columns right
        self.assertEqual(peaks['chr1'][0], {'start': 3660858,
                                            'end': 3661095,
                                            'chrom': 'chr1',
                                            'id': 'MACS_peak_1',
                                            'value': 40.8})

    def test_load_features_automatic(self):
        # load features
        peaks = load_features('test/peaks.bed')

        # check number of features loaded
        self.assertEqual(len(peaks['chr1']), 982)

        # check first feature to confirm we read all the columns right
        self.assertEqual(peaks['chr1'][0], {'start': 3660858,
                                            'end': 3661095,
                                            'chrom': 'chr1',
                                            'id': 'MACS_peak_1',
                                            'value': 40.8})

    def test_load_features_from_bedgraph_with_value(self):
        # load features
        peaks = load_features('test/reduced_bedgraph.bed', value_index=3)

        # check number of features loaded
        self.assertEqual(len(peaks['chr4']), 1299)

        # check first feature to confirm we read all the columns right
        self.assertEqual(peaks['chr4'][0], {'start': 54900600,
                                            'end': 54905749,
                                            'chrom': 'chr4',
                                            'value': 1.0})

    def test_load_features_from_bedgraph_automatic(self):
        # load features
        peaks = load_features('test/reduced_bedgraph.bed')

        # check number of features loaded
        self.assertEqual(len(peaks['chr4']), 1299)

        # check first feature to confirm we read all the columns right
        self.assertEqual(peaks['chr4'][0], {'start': 54900600,
                                            'end': 54905749,
                                            'chrom': 'chr4',
                                            'value': 1.0})
