"""
Unit tests for lib5c.parsers.scaled.
"""

import unittest

from lib5c.parsers.scaled import load_scaled


class ScaledTestCase(unittest.TestCase):
    def test_load_scaled(self):
        # load counts
        counts = load_scaled('test/test.scaled')

        # check keys
        self.assertTrue('Klf4' in counts.keys())
        self.assertEqual(len(counts.keys()), 7)

        # check size of array
        self.assertEqual(len(counts['Klf4']), 82)
        self.assertEqual(len(counts['Klf4'][0]), 82)

        # check one count value
        self.assertEqual(counts['Klf4'][0][0], 6.4239)

        # check symmetry of matrix
        self.assertEqual(counts['Klf4'][1][0], 5.3462)
        self.assertEqual(counts['Klf4'][0][1], 5.3462)
