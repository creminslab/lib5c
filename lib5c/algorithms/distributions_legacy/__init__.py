"""
Subpackage for fitting distributions to 5C data and using them to assign
p-values to 5C interactions.
"""
