"""
Unit tests for ``lib5c.algorithms.variance.grouping``.
"""

import unittest

from lib5c.parsers.primers import load_primermap
from lib5c.parsers.counts import load_counts


class GroupingTestCase(unittest.TestCase):
    def setUp(self):
        # set up some matrices
        self.primermap = load_primermap('test/bins_new.bed')
        self.real_obs_counts = load_counts('test/test_new.counts',
                                           self.primermap)['Sox2']
        # self.real_exp_counts = load_counts(test/test_new_exp.counts')['Sox2']

    def test_group_by_expected_value(self):
        pass


if __name__ == '__main__':
    unittest.main()
