import unittest
import subprocess


class PlotHeatmapTestCase(unittest.TestCase):
    def test_fragment_heatmap(self):
        cmd = 'lib5c plot heatmap ' \
            '-p test/primers.bed -r Sox2 -C ' \
            'test/test_raw_cis.counts test/test_raw_cis_Sox2.png'
        self.assertEqual(subprocess.call(cmd, shell=True), 0)

    def test_bin_heatmap(self):
        cmd = 'lib5c plot heatmap ' \
            '-p test/bins.bed -r Sox2 -g mm9 -RC -d test/communities.bed ' \
            '-c obs_over_exp ' \
            'test/test.counts test/test_Sox2.png'
        self.assertEqual(subprocess.call(cmd, shell=True), 0)

    def test_absolute_heatmap(self):
        cmd = 'lib5c plot heatmap ' \
              '-p test/bins.bed -r Sox2 -g mm9 -RC -s obs ' \
              'test/test.counts test/test_Sox2_abs.png'
        self.assertEqual(subprocess.call(cmd, shell=True), 0)

    def test_pvalue_heatmap(self):
        cmd = 'lib5c plot heatmap ' \
              '-p test/bins.bed -r Sox2 -g mm9 -PRC ' \
              'test/pvalues.counts test/pvalues_Sox2.png'
        self.assertEqual(subprocess.call(cmd, shell=True), 0)

    def test_tetris_heatmap(self):
        cmd = 'lib5c plot heatmap ' \
              '-p test/bins_new.bed -r Sox2 -g mm9 -TRC ' \
              'test/colors.counts test/colors_Sox2.png'
        self.assertEqual(subprocess.call(cmd, shell=True), 0)

    def test_zoom_heatmap_bigwig(self):
        cmd = 'lib5c plot heatmap ' \
            '-p test/bins.bed -r Sox2 -g mm9 -RC -c obs_over_exp ' \
            "-x chr3:34955000-35050000 -y chr3:34510000-34605000 " \
            '-t test/bedgraph.bw ' \
            'test/test.counts test/test_Sox2_zoom_bigwig.png'
        self.assertEqual(subprocess.call(cmd, shell=True), 0)

    def test_zoom_heatmap_bedgraph(self):
        cmd = 'lib5c plot heatmap ' \
            '-p test/bins.bed -r Sox2 -g mm9 -RC -c obs_over_exp ' \
            "-x chr3:34955000-35050000 -y chr3:34510000-34605000 " \
            '-t test/bedgraph.bed ' \
            'test/test.counts test/test_Sox2_zoom_bedgraph.png'
        self.assertEqual(subprocess.call(cmd, shell=True), 0)

    def test_zoom_heatmap_bed(self):
        cmd = 'lib5c plot heatmap ' \
            '-p test/bins.bed -r Sox2 -g mm9 -RC -c obs_over_exp ' \
            "-x chr3:34955000-35050000 -y chr3:34510000-34605000 " \
            '-t test/annotations/V65EScells_CTCFMed12Smc1_2015.bed ' \
            'test/test.counts test/test_Sox2_zoom_bed.png'
        self.assertEqual(subprocess.call(cmd, shell=True), 0)

    def test_zoom_heatmap_consensus(self):
        cmd = 'lib5c plot heatmap ' \
            '-p test/bins.bed -r Sox2 -RC -c obs_over_exp ' \
            "-x chr3:34955000-35050000 -y chr3:34510000-34605000 " \
            '-g test/CTCF_consensus.bed ' \
            'test/test.counts test/test_Sox2_zoom_consensus.png'
        self.assertEqual(subprocess.call(cmd, shell=True), 0)
