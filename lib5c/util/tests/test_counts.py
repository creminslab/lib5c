"""
Unit tests for lib5c.util.counts.
"""

import unittest

import numpy as np

from lib5c.parsers.counts import load_counts
from lib5c.parsers.primers import load_primermap
from lib5c.util.counts import flatten_regional_counts,\
    unflatten_regional_counts, flatten_counts, unflatten_counts,\
    flatten_counts_to_list, unflatten_counts_from_list,\
    calculate_regional_pvalues, calculate_pvalues


class CountsTestCase(unittest.TestCase):
    def setUp(self):
        # get some counts
        self.pixelmap = load_primermap('test/bins.bed')
        del self.pixelmap['Oct4-s']
        self.counts = load_counts('test/ES_Rep1_obs_over_exp.counts',
                                  self.pixelmap)

    def test_flatten_unflatten_regional_counts(self):
        # flatten and unflatten some counts
        flattened_counts = flatten_regional_counts(self.counts['Sox2'])
        unflattened_counts = unflatten_regional_counts(flattened_counts)

        # check equality of all interactions
        self.assertTrue(np.array_equal(self.counts['Sox2'], unflattened_counts))

    def test_flatten_unflatten_counts(self):
        # flatten and unflatten some counts
        flattened_counts = flatten_counts(self.counts)
        unflattened_counts = unflatten_counts(flattened_counts)

        # check number of keys
        self.assertEqual(len(self.counts.keys()),
                         len(unflattened_counts.keys()))

        # check equality of keys
        for key in self.counts.keys():
            self.assertTrue(key in unflattened_counts.keys())

        # check equality of all interactions
        for region in self.counts.keys():
            self.assertTrue(np.array_equal(self.counts[region],
                                           unflattened_counts[region]))

    def test_flatten_unflatten_counts_to_list(self):
        # flatten and unflatten som[0]e counts
        region_order = list(self.counts.keys())
        flattened_list = flatten_counts_to_list(self.counts, region_order)
        unflattened_counts = unflatten_counts_from_list(
            flattened_list, region_order, self.pixelmap)

        # check number of keys
        self.assertEqual(len(self.counts.keys()),
                         len(unflattened_counts.keys()))

        # check equality of keys
        for key in self.counts.keys():
            self.assertTrue(key in unflattened_counts.keys())

        # check equality of all interactions
        for region in self.counts.keys():
            self.assertTrue(np.array_equal(self.counts[region],
                                           unflattened_counts[region]))

    def test_calculate_regional_pvalues(self):
        # load some known pvalues
        known_pvalues = load_counts('test/ES_Rep1_obs_over_exp_pvalues.counts',
                                    self.pixelmap)

        for region in self.counts.keys():
            # model some pvalues
            params, test_pvalues = calculate_regional_pvalues(
                self.counts[region])

            # see https://bitbucket.org/creminslab/5c_pipeline/issues/1
            self.assertTrue(np.allclose(known_pvalues[region], test_pvalues,
                                        atol=0.00001))

    def test_calculate_pvalues(self):
        # load some known pvalues
        known_pvalues = load_counts('test/ES_Rep1_obs_over_exp_pvalues.counts',
                                    self.pixelmap)

        # model some pvalues
        test_pvalues, stats_dict = calculate_pvalues(self.counts)

        for region in self.counts.keys():
            # see https://bitbucket.org/creminslab/5c_pipeline/issues/1
            self.assertTrue(np.allclose(known_pvalues[region],
                                        test_pvalues[region], atol=0.00001))
