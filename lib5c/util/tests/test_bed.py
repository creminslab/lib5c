"""
Unit tests for lib5c.util.bed.
"""

import unittest

from lib5c.parsers.bed import load_features
from lib5c.util.bed import count_intersections, flatten_features,\
    check_intersect


class BedTestCase(unittest.TestCase):
    def setUp(self):
        # get some features
        self.features_a = load_features(
            'test/annotations/V65EScells_CTCFMed12Smc1_2015.bed')
        self.features_b = load_features(
            'test/annotations/V65EScells_Superenhancers_1_2015.bed')

    def test_flatten_features(self):
        # flatten features
        flat_features = flatten_features(self.features_a)

        # confirm data structure
        self.assertEqual(type(flat_features), list)
        self.assertEqual(type(flat_features[0]), dict)

        # confirm presence of feature in list
        self.assertTrue({'start': 3163937, 'end': 3164420, 'chrom': 'chr7'}
                        in flat_features)

    def test_check_intersect(self):
        # confirm presence and absence of known intersections in feature sets
        self.assertTrue(check_intersect(self.features_a['chr7'][1],
                                        self.features_b['chr7'][0]))
        self.assertFalse(check_intersect(self.features_a['chr7'][0],
                                         self.features_b['chr7'][0]))

    def test_count_intersections(self):
        # flatten one feature set
        flat_features = flatten_features(self.features_a)

        # test count_intersections
        self.assertEqual(count_intersections(self.features_b['chr7'][0],
                                             flat_features), 3)


if __name__ == '__main__':
    unittest.main()
