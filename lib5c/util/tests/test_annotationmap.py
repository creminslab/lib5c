"""
Unit tests for lib5c.util.bed.
"""

import unittest

from lib5c.parsers.primers import load_primermap
from lib5c.parsers.bed import load_features
from lib5c.util.annotationmap import make_annotationmaps,\
    make_single_annotationmap


class AnnotationmapTestCase(unittest.TestCase):
    def setUp(self):
        # we'll need a pixelmap
        self.pixelmap = load_primermap('test/bins.bed')

    def test_make_single_annotationmap(self):
        # find an annotation to use for annotationmap creation
        filename = 'test/annotations/V65EScells_CTCFMed12Smc1_2015.bed'

        # load features from the annotation file
        features = load_features(filename)

        # make annotationmap
        annotationmap = make_single_annotationmap(features, self.pixelmap)

        # check annotationmap contents
        self.assertEqual(annotationmap['Sox2'][45], 1)

    def test_make_annotationmaps(self):
        # directory where we should look for annotations
        directory = 'test/annotations'

        # make the annotationmaps
        annotationmaps = make_annotationmaps(self.pixelmap, directory)

        # check annotationmaps.keys()
        self.assertTrue('V65EScells_CTCFMed12Smc1_2015'
                        in annotationmaps.keys())

        # check annotatiomap contents
        self.assertEqual(
            annotationmaps['V65EScells_CTCFMed12Smc1_2015']['Sox2'][45], 1)
        self.assertEqual(annotationmaps['wildcard']['Sox2'][45], 100)


if __name__ == '__main__':
    unittest.main()
