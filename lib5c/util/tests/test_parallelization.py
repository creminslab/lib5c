"""
Unit tests for lib5c.util.parallelization.
"""

import unittest
import sys


@unittest.skipIf(sys.platform.startswith("win"), "will not run on Windows")
class ParallelizationTestCase(unittest.TestCase):
    def test_function_one_nonparallel(self):
        from lib5c.util.parallelization import test_function_one
        self.assertEqual(test_function_one(5),
                         10)

    def test_function_one_parallel(self):
        from lib5c.util.parallelization import test_function_one
        self.assertEqual(test_function_one({'a': 4, 'b': 6}),
                         {'a': 8, 'b': 12})

    def test_function_two_nonparallel(self):
        from lib5c.util.parallelization import test_function_two
        self.assertEqual(test_function_two(5),
                         20)

    def test_function_two_parallel(self):
        from lib5c.util.parallelization import test_function_two
        self.assertEqual(test_function_two({'a': 4, 'b': 6}),
                         {'a': 16, 'b': 24})

    def test_function_two_constant_kwarg(self):
        from lib5c.util.parallelization import test_function_two
        self.assertEqual(test_function_two({'a': 4, 'b': 6}, multiplier=3),
                         {'a': 12, 'b': 18})

    def test_function_two_variable_kwarg(self):
        from lib5c.util.parallelization import test_function_two
        self.assertEqual(test_function_two({'a': 4, 'b': 6},
                                           multiplier={'a': 5, 'b': 10}),
                         {'a': 20, 'b': 60})

    def test_function_three_nonparallel(self):
        from lib5c.util.parallelization import test_function_three
        p, s = test_function_three(3, 4)
        self.assertEqual(p, 12)
        self.assertEqual(s, 7)

    def test_function_three_parallel(self):
        from lib5c.util.parallelization import test_function_three
        p, s = test_function_three({'a': 4, 'b': 6}, 9)
        self.assertEqual(p, {'a': 36, 'b': 54})
        self.assertEqual(s, {'a': 13, 'b': 15})

    def test_function_three_variable_second_arg(self):
        from lib5c.util.parallelization import test_function_three
        p, s = test_function_three({'a': 4, 'b': 6}, {'a': 4, 'b': 6})
        self.assertEqual(p, {'a': 16, 'b': 36})
        self.assertEqual(s, {'a': 8, 'b': 12})

    def test_function_four_nonparallel(self):
        from lib5c.util.parallelization import test_function_four
        self.assertEqual(test_function_four(7, {'s': 1, 't': -1}), 7)

    def test_function_four_parallel(self):
        from lib5c.util.parallelization import test_function_four
        self.assertEqual(
            test_function_four({'a': 6, 'b': 5},
                               {'s': {'a': 10, 'b': -10},
                                't': {'a': -100, 'b': 100}}),
            {'a': -84, 'b': 95})
