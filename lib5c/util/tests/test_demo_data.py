"""
Unit tests for lib5c.util.bed.
"""

import os
import shutil
import unittest

from luigi.configuration.cfg_parser import LuigiConfigParser

from lib5c.contrib.luigi.config import drop_config_file
from lib5c.util.demo_data import ensure_demo_data, edit_demo_config, DEMO_FILES


class DemoDataTestCase(unittest.TestCase):
    def clean(self):
        # delete input folder if it exists
        if os.path.exists('input'):
            shutil.rmtree('input')

        # delete a config file if it exists
        if os.path.exists('luigi.cfg'):
            os.remove('luigi.cfg')

    def assert_file_nonempty(self, file):
        self.assertTrue(os.path.exists(file))
        self.assertTrue(os.stat(file).st_size > 0)

    def setUp(self):
        self.clean()
        drop_config_file()

    def test_ensure_demo_data(self):
        ensure_demo_data()
        for _, f in DEMO_FILES:
            self.assert_file_nonempty(os.path.join('input', f))

    def test_edit_demo_config(self):
        edit_demo_config()
        config = LuigiConfigParser()
        config.read('luigi.cfg')
        self.assertEqual(
            config.get('PrimerFile', 'primerfile'),
            'input/BED_ES-NPC-iPS-LOCI_mm9.bed'
        )

    def tearDown(self):
        self.clean()
