"""
Unit tests for lib5c.util.bed.
"""

import unittest

from lib5c.parsers.bed import load_features
from lib5c.parsers.primers import load_primermap
from lib5c.util.bedgraph import reduce_bedgraph


class BedgraphTestCase(unittest.TestCase):
    def setUp(self):
        # make a pixelmap
        self.pixelmap = load_primermap('test/bins.bed')

        # load features from a bedgraph file
        self.bedgraph_features = load_features('test/bedgraph_small.bed')

    def test_reduce_bedgraph(self):
        # make reduced bedgraph
        reduced_bedgraph_features = reduce_bedgraph('test/bedgraph_small.bed',
                                                    self.pixelmap)

        # make assertions
        self.assertEqual(len(self.bedgraph_features['chr4']), 199459)
        self.assertEqual(len(reduced_bedgraph_features['chr4']), 1299)
        self.assertEqual(self.bedgraph_features['chr4'][0]['value'], 2.0)
        self.assertEqual(reduced_bedgraph_features['chr4'][0]['value'], 1.0)


if __name__ == '__main__':
    unittest.main()
